
import React, { Component } from 'react';
import {SafeAreaView, StyleSheet, View, Text, TextInput, Button} from 'react-native';

import {Header,LearnMoreLinks,Colors,DebugInstructions,ReloadInstructions} from 'react-native/Libraries/NewAppScreen';


export default class App extends Component {
  state={
    placeName: "",
    places: []
  }

  plalceNameChangeHandler = val => {
    this.setState({
      placeName: val
    });
  };

  placeSubmitHandler = () => {
    if(this.state.placeName.trim() ===""){
      return;
    }
    this.setState(prevState => {
      return{
        places: prevState.places.concat(prevState.placeName)
      }
    })
  }
  render(){
    const placeOutput = this.state.places.map(place => (
      <Text>{place}</Text>
    ));
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <TextInput
            placeholder="An Awesome Place"
            value={this.state.placeName}
            onChangeText={this.plalceNameChangeHandler}
            style={styles.placeInput}
          />
          <Button title="Add" style={styles.placeButton} onPress={this.placeSubmitHandler} />
        </View>
        <View>
        <View>{placeOutput}</View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 20,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: "flex-start", 
  },
  inputContainer:{
    //flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between', 
    alignItems: 'center',  
  },
  placeInput:{
    width: '70%',
  },
  placeButton:{
    width: '30%'
  }




  
});

